//
//  DuLieuGhiLoObject.m
//  Loto
//
//  Created by Dai Trinh on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "DuLieuGhiLoObject.h"

@implementation DuLieuGhiLoObject

- (instancetype) initWithName: (NSString *) name
                       number: (NSString *) number
                        point: (NSString *) point
                         date: (NSString *) date
{
    self = [super init];
    
    if (self) {
        _name = name;
        _number = number;
        _point = point;
        _date = date;
    }
    return self;
}

@end
