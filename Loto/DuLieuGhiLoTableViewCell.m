//
//  DuLieuGhiDeTableViewCell.m
//  Loto
//
//  Created by Dai Trinh on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "DuLieuGhiLoTableViewCell.h"

@implementation DuLieuGhiLoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) displayCellWithObject : (DuLieuGhiLoObject *) duLieuGhiLoObject {
    _viewLine1.backgroundColor = kColorGrey300;
    _viewLine2.backgroundColor = kColorGrey300;
    _viewLine3.backgroundColor = kColorGrey300;
    
    _lblName.text = duLieuGhiLoObject.name;
    _lblNumber.text = duLieuGhiLoObject.number;
    _lblPoint.text = duLieuGhiLoObject.point;
    _lblDate.text = duLieuGhiLoObject.date;
}

@end
