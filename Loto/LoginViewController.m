//
//  LoginViewController.m
//  Loto
//
//  Created by Chính Nguyen on 7/3/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "LoginViewController.h"
#import "Constraint.h"
#import "AFNetWorking.h"
#import "ProgressHUD.h"
#import "TabBarViewController.h"
#import "AppDelegate.h"
#import "XMLReader.h"
//#import "TabbarAdminViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];
    gestureRecognizer.cancelsTouchesInView = NO;
    
    NSString *phoneNumber = [[NSUserDefaults standardUserDefaults]
                             stringForKey:@"PhoneNumber"];
    //    self.fieldPhoneNumber.text = phoneNumber;
    //    self.fieldPhoneNumber.text = @"agency.manager1@thehegeo.com";
    
    self.fieldPhoneNumber.text = @"chinhnd";
    //    self.fieldPhoneNumber.text = @"agency.shipper4@thehegeo.com";
    self.fieldPassword.text = @"123456";
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self dismissKeyboard];
}
- (void)dismissKeyboard {
    [self.view endEditing: YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == _fieldPhoneNumber) [_fieldPassword becomeFirstResponder];
    if (textField == _fieldPassword) [self btnLogin:nil];
    return YES;
}

- (IBAction)btnLogin:(id)sender {
    NSString *phoneNumber = [self.fieldPhoneNumber.text lowercaseString];
    NSString *password = self.fieldPassword.text;
    if ([phoneNumber length] == 0)	{
        [ProgressHUD showError:@"Bạn chưa điền số điện thoại."];
        return;
    }
    if ([password length] == 0)	{
        [ProgressHUD showError:@"Bạn chưa điền mật khẩu."];
        return;
    }
    [ProgressHUD show:@"Vui lòng đợi..."];
    NSString *message = [[NSString alloc] initWithFormat:@"<?xml version=\"1.0\" ?>\n<user>\n<username>%@</username>\n<password>%@</password></user>",phoneNumber,password];

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:kURL, @"login"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu",(unsigned long)[message length]];
    
    [request addValue:@"application/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request addValue:msgLength                         forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[message dataUsingEncoding:NSUTF8StringEncoding]];
    NSData *response = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:nil error:nil];
    
//    NSLog(@"Response: %@",[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding]);
    
    NSDictionary* xmlDict = [XMLReader dictionaryForXMLString:[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding] error:nil];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:xmlDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSLog(@"Final %@",jsonString);
    }
    
    NSString *success = xmlDict[@"user"][@"success"][@"text"];
    success = [[success stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
   
   NSString *role = xmlDict[@"user"][@"role"][@"text"];
   role = [[role stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
   
   NSString *userName = xmlDict[@"user"][@"username"][@"text"];
   role = [[role stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    if([success isEqualToString:@"true"]){
       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
       TabBarViewController *tabBarViewController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
       AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
       ((AppDelegate *)([UIApplication sharedApplication].delegate)).role = role;
       ((AppDelegate *)([UIApplication sharedApplication].delegate)).userName = userName;
       ((AppDelegate *)([UIApplication sharedApplication].delegate)).password = password;
       //        NavigationController *navigationController =
       //        [[NavigationController alloc] initWithRootViewController:tabBarViewController];
       [self presentViewController:tabBarViewController
                          animated:YES
                        completion:^{
                        }];
       [ProgressHUD dismiss];
       [appDelegate.window makeKeyAndVisible];
    }
//    } else if([success isEqualToString:@"true"] && ([role isEqualToString:@"1"] || [role isEqualToString:@"0"])){
//       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//       TabBarViewController *tabBarViewController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
//       AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//       //        NavigationController *navigationController =
//       //        [[NavigationController alloc] initWithRootViewController:tabBarViewController];
//       [self presentViewController:tabBarViewController
//                          animated:YES
//                        completion:^{
//                        }];
//       [ProgressHUD dismiss];
//       [appDelegate.window makeKeyAndVisible];
//    }
    else{
        [ProgressHUD showError:@"Sai tên đăng nhập hoặc mật khẩu!"];
    }
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://10.20.22.108:8080/WSDemo/api/login"]];
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    manager.responseSerializer.acceptableContentTypes =  [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/xml"];
//    NSURLSessionDataTask *task = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
//        NSString *fetchedXML = [[NSString alloc] initWithData:(NSData *)responseObject encoding:NSUTF8StringEncoding];
//        NSLog(@"Response string: %@",fetchedXML);
//    }];
//    [task resume];
//    NSDictionary *body = @{@"password": password, @"username": phoneNumber, @"grant_type": @"password"};
//    NSLog(@"%@",body);
//    //    NSString *loginURL = [NSString stringWithFormat:kBaseUrl,@"token"];
//    NSString *loginURL = @"http://10.20.22.108:8080/WSDemo/api/login";
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//    [manager.requestSerializer setValue:@"application/xml; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
//    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
//    [manager POST:loginURL parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        
////        [[NSUserDefaults standardUserDefaults] setObject:phoneNumber forKey:@"PhoneNumber"];
////        ((AppDelegate *)([UIApplication sharedApplication].delegate)).token = [responseObject valueForKey:@"access_token"];
////        ((AppDelegate *)([UIApplication sharedApplication].delegate)).phoneNumber = self.fieldPhoneNumber.text;
//        self.fieldPhoneNumber.text = phoneNumber;
//        self.fieldPassword.text = nil;
//        NSLog(@"%@",responseObject);
//        
//        [ProgressHUD dismiss];
//        
//        
//                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                TabBarViewController *tabBarViewController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
//                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//        //        NavigationController *navigationController =
//        //        [[NavigationController alloc] initWithRootViewController:tabBarViewController];
//                [self presentViewController:tabBarViewController
//                                   animated:YES
//                                 completion:^{
//                                 }];
//                [appDelegate.window makeKeyAndVisible];
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
//        
//        NSLog(@"%@",ErrorResponse);
//        [ProgressHUD showError:@"Sai tên đăng nhập hoặc mật khẩu!"];
//    }];
}


- (IBAction)btnForgotPassword:(id)sender {
    NSLog(@"Forgot password");
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
