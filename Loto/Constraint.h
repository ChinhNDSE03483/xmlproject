//
//  Constraint.h
//  Loto
//
//  Created by Dai Trinh on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#ifndef Constraint_h
#define Constraint_h

#define kURL                @"http://192.168.137.1:8080/WSDemo/api/%@"
#define kColorGrey300       [UIColor colorWithRed:224.f/255.f green:224.f/255.f blue:224.f/255.f alpha:1]

#endif /* Constraint_h */
