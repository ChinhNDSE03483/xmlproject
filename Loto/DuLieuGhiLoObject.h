//
//  DuLieuGhiLoObject.h
//  Loto
//
//  Created by Dai Trinh on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DuLieuGhiLoObject : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *point;
@property (nonatomic, strong) NSString *date;

- (instancetype) initWithName: (NSString *) name
                       number: (NSString *) number
                        point: (NSString *) point
                         date: (NSString *) date;

@end
