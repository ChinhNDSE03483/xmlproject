//
//  FadeAnimationController.h
//  youtube
//
//  Created by Chính Nguyen on 5/22/17.
//  Copyright © 2017 Chính Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface FadeAnimationController : NSObject<UIViewControllerAnimatedTransitioning>
@property (nonatomic) CGFloat transitionDuration;
@end
