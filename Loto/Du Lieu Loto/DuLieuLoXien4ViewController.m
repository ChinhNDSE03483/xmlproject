//
//  DuLieuLoXien4ViewController.m
//  Loto
//
//  Created by Chính Nguyen on 7/5/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "DuLieuLoXien4ViewController.h"

@interface DuLieuLoXien4ViewController ()

@end

@implementation DuLieuLoXien4ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _tbvLoXien4.delegate = self;
    _tbvLoXien4.dataSource = self;
    _tbvLoXien4.tableFooterView = [[UIView alloc] init];
    
    _viewLine1.backgroundColor = kColorGrey300;
    _viewLine2.backgroundColor = kColorGrey300;
    _viewLine3.backgroundColor = kColorGrey300;
    _viewLine4.backgroundColor = kColorGrey300;
    _viewLine5.backgroundColor = kColorGrey300;
    _viewLine6.backgroundColor = kColorGrey300;
    
    _xmlDict = [[NSDictionary alloc] init];
    _listLoXien4 = [[NSMutableArray alloc] init];
    
    [self getDataFromServer];
    NSLog(@"Data Count: %ld", [_listLoXien4 count]);
    [self reloadMethod];
}

-(void)reloadMethod{
    [self getDataFromServer];
    [self.tbvLoXien4 reloadData];
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 4.0 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self reloadMethod];
    });
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_listLoXien4 count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"DuLieuLoXien4TableViewCell";
    DuLieuLoXien4TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"DuLieuLoXien4TableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    DuLieuLoXien4Object *duLieuLoXien4Object = _listLoXien4[indexPath.row];
    
    [cell displayCellWithObject:duLieuLoXien4Object];
    
    cell.preservesSuperviewLayoutMargins = NO;
    cell.separatorInset = UIEdgeInsetsZero;
    cell.layoutMargins = UIEdgeInsetsZero;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (void) getDataFromServer {
    NSString *message = [[NSString alloc] initWithFormat:@"<?xml version=\"1.0\" ?>\n<requestData>\n<username>%@</username></requestData>",@"tungnt"];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:kURL, @"get/lo_xien_4/all"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu",(unsigned long)[message length]];
    
    [request addValue:@"application/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request addValue:msgLength                         forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[message dataUsingEncoding:NSUTF8StringEncoding]];
    NSData *response = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:nil error:nil];
    
    //    NSLog(@"Response Ghi De: %@",[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding]);
    
    _xmlDict = [XMLReader dictionaryForXMLString:[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding] error:nil];
    
    [_listLoXien4 removeAllObjects];
    
    for (NSDictionary *dict in _xmlDict[@"list_lo_xien_4"][@"lo_xien_4"]) {
        
        NSString *name = [[[NSString stringWithFormat:@"%@", dict[@"username"][@"text"]]
                           stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                          stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *number1 = [[[NSString stringWithFormat:@"%@", dict[@"number1"][@"text"]]
                              stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                             stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *number2 = [[[NSString stringWithFormat:@"%@", dict[@"number2"][@"text"]]
                              stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                             stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *number3 = [[[NSString stringWithFormat:@"%@", dict[@"number3"][@"text"]]
                              stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                             stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *number4 = [[[NSString stringWithFormat:@"%@", dict[@"number4"][@"text"]]
                              stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                             stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *point = [[[NSString stringWithFormat:@"%@", dict[@"point"][@"text"]]
                            stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                           stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *date = [[[NSString stringWithFormat:@"%@", dict[@"date"][@"text"]]
                           stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                          stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DuLieuLoXien4Object *duLieuLoXien4Object = [[DuLieuLoXien4Object alloc] initWithName:name
                                                                                     number1:number1
                                                                                     number2:number2
                                                                                     number3:number3
                                                                                     number4:number4
                                                                                       point:point
                                                                                        date:date];
        [_listLoXien4 addObject:duLieuLoXien4Object];
    }
    
}

@end
