//
//  DuLieuLoXien3ViewController.h
//  Loto
//
//  Created by Chính Nguyen on 7/5/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constraint.h"
#import <AFNetworking.h>
#import <ProgressHUD.h>
#import "XMLReader.h"
#import "DuLieuLoXien3TableViewCell.h"

@interface DuLieuLoXien3ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tbvLoXien3;
@property (weak, nonatomic) IBOutlet UIView *viewLine1;
@property (weak, nonatomic) IBOutlet UIView *viewLine2;
@property (weak, nonatomic) IBOutlet UIView *viewLine3;
@property (weak, nonatomic) IBOutlet UIView *viewLine4;
@property (weak, nonatomic) IBOutlet UIView *viewLine5;

@property (strong, nonatomic) NSDictionary *xmlDict;
@property (strong, nonatomic) NSMutableArray *listLoXien3;

@end
