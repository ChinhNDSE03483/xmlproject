//
//  DuLieuGhiLoViewController.m
//  Loto
//
//  Created by Chính Nguyen on 7/5/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "DuLieuGhiLoViewController.h"

@interface DuLieuGhiLoViewController ()

@end

@implementation DuLieuGhiLoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _tbvLo.delegate = self;
    _tbvLo.dataSource = self;
    _tbvLo.tableFooterView = [[UIView alloc] init];
    
    _viewLine1.backgroundColor = kColorGrey300;
    _viewLine2.backgroundColor = kColorGrey300;
    _viewLine3.backgroundColor = kColorGrey300;
    
    _xmlDict = [[NSDictionary alloc] init];
    _listLo = [[NSMutableArray alloc] init];
    
    [self getDataFromServer];
    NSLog(@"Data Count: %ld", [_listLo count]);
    [self reloadMethod];
}
-(void)reloadMethod{
    [self getDataFromServer];
    [self.tbvLo reloadData];
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 4.0 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self reloadMethod];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_listLo count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"DuLieuGhiLoTableViewCell";
    DuLieuGhiLoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"DuLieuGhiLoTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    DuLieuGhiLoObject *duLieuGhiLoObject = _listLo[indexPath.row];
    
    [cell displayCellWithObject:duLieuGhiLoObject];
    
    cell.preservesSuperviewLayoutMargins = NO;
    cell.separatorInset = UIEdgeInsetsZero;
    cell.layoutMargins = UIEdgeInsetsZero;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (void) getDataFromServer {
    NSString *message = [[NSString alloc] initWithFormat:@"<?xml version=\"1.0\" ?>\n<requestData>\n<username>%@</username></requestData>",@"tungnt"];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:kURL, @"get/lo/all"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu",(unsigned long)[message length]];
    
    [request addValue:@"application/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request addValue:msgLength                         forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[message dataUsingEncoding:NSUTF8StringEncoding]];
    NSData *response = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:nil error:nil];
    
    //    NSLog(@"Response Ghi De: %@",[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding]);
    
    _xmlDict = [XMLReader dictionaryForXMLString:[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding] error:nil];
    
    [_listLo removeAllObjects];
    
    for (NSDictionary *dict in _xmlDict[@"list_lo"][@"lo"]) {
        
        NSString *name = [[[NSString stringWithFormat:@"%@", dict[@"username"][@"text"]]
                           stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                          stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *number = [[[NSString stringWithFormat:@"%@", dict[@"number"][@"text"]]
                             stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                            stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *point = [[[NSString stringWithFormat:@"%@", dict[@"point"][@"text"]]
                            stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                           stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *date = [[[NSString stringWithFormat:@"%@", dict[@"date"][@"text"]]
                           stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                          stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        DuLieuGhiLoObject *duLieuGhiLoObject = [[DuLieuGhiLoObject alloc] initWithName:name
                                                                                number:number
                                                                                 point:point
                                                                                  date:date];
        [_listLo addObject:duLieuGhiLoObject];
    }
    
}

@end
