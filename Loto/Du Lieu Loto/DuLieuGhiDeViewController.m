//
//  DuLieuGhiDeViewController.m
//  Loto
//
//  Created by Chính Nguyen on 7/5/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "DuLieuGhiDeViewController.h"

@interface DuLieuGhiDeViewController ()

@end

@implementation DuLieuGhiDeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _tbvGhiDe.delegate = self;
    _tbvGhiDe.dataSource = self;
    _tbvGhiDe.tableFooterView = [[UIView alloc] init];
    
    _viewLine1.backgroundColor = kColorGrey300;
    _viewLine2.backgroundColor = kColorGrey300;
    _viewLine3.backgroundColor = kColorGrey300;
    
    _xmlDict = [[NSDictionary alloc] init];
    _listDe = [[NSMutableArray alloc] init];
    
    [self getDataFromServer];
    NSLog(@"Data Count: %ld", [_listDe count]);
    [self reloadMethod];
}
-(void)reloadMethod{
    [self getDataFromServer];
    [self.tbvGhiDe reloadData];
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 4.0 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self reloadMethod];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_listDe count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"DuLieuGhiDeTableViewCell";
    DuLieuGhiDeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"DuLieuGhiDeTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    DuLieuGhiDeObject *duLieuGhiDeObject = _listDe[indexPath.row];
    
    [cell displayCellWithObject:duLieuGhiDeObject];
    
    cell.preservesSuperviewLayoutMargins = NO;
    cell.separatorInset = UIEdgeInsetsZero;
    cell.layoutMargins = UIEdgeInsetsZero;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (void) getDataFromServer {
    NSString *message = [[NSString alloc] initWithFormat:@"<?xml version=\"1.0\" ?>\n<requestData>\n<username>%@</username></requestData>",@"tungnt"];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:kURL, @"get/de/all"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu",(unsigned long)[message length]];
    
    [request addValue:@"application/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request addValue:msgLength                         forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[message dataUsingEncoding:NSUTF8StringEncoding]];
    NSData *response = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:nil error:nil];
    
    //    NSLog(@"Response Ghi De: %@",[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding]);
    
    _xmlDict = [XMLReader dictionaryForXMLString:[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding] error:nil];
    
    [_listDe removeAllObjects];
    
    for (NSDictionary *dict in _xmlDict[@"list_de"][@"de"]) {
        
        NSString *name = [[[NSString stringWithFormat:@"%@", dict[@"username"][@"text"]]
                           stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                          stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *number = [[[NSString stringWithFormat:@"%@", dict[@"number"][@"text"]]
                             stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                            stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *price = [[[NSString stringWithFormat:@"%@", dict[@"price"][@"text"]]
                            stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                           stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *date = [[[NSString stringWithFormat:@"%@", dict[@"date"][@"text"]]
                           stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                          stringByReplacingOccurrencesOfString:@" " withString:@""];
        DuLieuGhiDeObject *duLieuGhiDeObject = [[DuLieuGhiDeObject alloc] initWithName:name
                                                                                number:number
                                                                                 price:price
                                                                                  date:date];
        [_listDe addObject:duLieuGhiDeObject];
    }
    
}

@end
