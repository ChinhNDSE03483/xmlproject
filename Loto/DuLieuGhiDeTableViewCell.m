//
//  DuLieuGhiDeTableViewCell.m
//  Loto
//
//  Created by Dai Trinh on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "DuLieuGhiDeTableViewCell.h"

@implementation DuLieuGhiDeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) displayCellWithObject : (DuLieuGhiDeObject *) duLieuGhiDeObject {
    _viewLine1.backgroundColor = kColorGrey300;
    _viewLine2.backgroundColor = kColorGrey300;
    _viewLine3.backgroundColor = kColorGrey300;
    
    _lblName.text = duLieuGhiDeObject.name;
    _lblNumber.text = duLieuGhiDeObject.number;
    _lblPrice.text = duLieuGhiDeObject.price;
    _lblDate.text = duLieuGhiDeObject.date;
}

@end
