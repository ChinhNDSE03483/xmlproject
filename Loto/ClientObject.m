//
//  ClientObject.m
//  Loto
//
//  Created by Chính Nguyen on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "ClientObject.h"

@implementation ClientObject
- (instancetype) initWithUsername: (NSString *) username
                         password: (NSString *) password
                         fullName: (NSString *) fullName
                      phoneNumber: (NSString *) phoneNumber
                             role: (NSString *) role
                          success: (NSString *) success
                        createdBy: (NSString *) createdBy;
{
    self = [super init];
    
    if (self) {
        _username = username;
        _password = password;
        _fullName = fullName;
        _phoneNumber = phoneNumber;
        _role = role;
        _success = success;
        _createdBy = createdBy;
    }
    return self;
}
@end
