//
//  DuLieuGhiDeTableViewCell.m
//  Loto
//
//  Created by Dai Trinh on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "DuLieuLoXien2TableViewCell.h"

@implementation DuLieuLoXien2TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) displayCellWithObject : (DuLieuLoXien2Object *) duLieuLoXien2Object {
    _viewLine1.backgroundColor = kColorGrey300;
    _viewLine2.backgroundColor = kColorGrey300;
    _viewLine3.backgroundColor = kColorGrey300;
    _viewLine4.backgroundColor = kColorGrey300;
    
    _lblName.text = duLieuLoXien2Object.name;
    _lblNumber1.text = duLieuLoXien2Object.number1;
    _lblNumber2.text = duLieuLoXien2Object.number2;
    _lblPoint.text = duLieuLoXien2Object.point;
    _lblDate.text = duLieuLoXien2Object.date;
}

@end
