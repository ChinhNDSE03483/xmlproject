//
//  ClientTableViewCell.m
//  Loto
//
//  Created by Chính Nguyen on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "ClientTableViewCell.h"

@implementation ClientTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void) displayCellWithObject : (ClientObject *) clientObject {
    _viewCenter.backgroundColor = kColorGrey300;
    
    _lblName.text = clientObject.fullName;
    _lblPhone.text = clientObject.phoneNumber;
}
@end
