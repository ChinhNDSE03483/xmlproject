//
//  DuLieuGhiDeObject.h
//  Loto
//
//  Created by Dai Trinh on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DuLieuGhiDeObject : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *date;

- (instancetype) initWithName: (NSString *) name
                       number: (NSString *) number
                        price: (NSString *) price
                         date: (NSString *) date;

@end
