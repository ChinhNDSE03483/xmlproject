//
//  DuLieuGhiDeTableViewCell.h
//  Loto
//
//  Created by Dai Trinh on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DuLieuLoXien3Object.h"
#import "Constraint.h"

@interface DuLieuLoXien3TableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber1;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber2;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber3;
@property (weak, nonatomic) IBOutlet UILabel *lblPoint;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (weak, nonatomic) IBOutlet UIView *viewLine1;
@property (weak, nonatomic) IBOutlet UIView *viewLine2;
@property (weak, nonatomic) IBOutlet UIView *viewLine3;
@property (weak, nonatomic) IBOutlet UIView *viewLine4;
@property (weak, nonatomic) IBOutlet UIView *viewLine5;

- (void) displayCellWithObject : (DuLieuLoXien3Object *) duLieuLoXien3Object;

@end
