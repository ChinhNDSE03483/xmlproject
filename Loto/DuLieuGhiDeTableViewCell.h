//
//  DuLieuGhiDeTableViewCell.h
//  Loto
//
//  Created by Dai Trinh on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DuLieuGhiDeObject.h"
#import "Constraint.h"

@interface DuLieuGhiDeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (weak, nonatomic) IBOutlet UIView *viewLine1;
@property (weak, nonatomic) IBOutlet UIView *viewLine2;
@property (weak, nonatomic) IBOutlet UIView *viewLine3;

- (void) displayCellWithObject : (DuLieuGhiDeObject *) duLieuGhiDeObject;

@end
