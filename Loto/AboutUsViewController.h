//
//  AboutUsViewController.h
//  Loto
//
//  Created by Chính Nguyen on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutUsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnDone;
- (IBAction)btnDone:(id)sender;

@end
