//
//  DuLieuLoXien4Object.h
//  Loto
//
//  Created by Dai Trinh on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DuLieuLoXien4Object : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *number1;
@property (nonatomic, strong) NSString *number2;
@property (nonatomic, strong) NSString *number3;
@property (nonatomic, strong) NSString *number4;
@property (nonatomic, strong) NSString *point;
@property (nonatomic, strong) NSString *date;

- (instancetype) initWithName: (NSString *) name
                      number1: (NSString *) number1
                      number2: (NSString *) number2
                      number3: (NSString *) number3
                      number4: (NSString *) number4
                        point: (NSString *) point
                         date: (NSString *) date;

@end
