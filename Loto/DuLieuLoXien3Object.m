//
//  DuLieuLoXien3Object.m
//  Loto
//
//  Created by Dai Trinh on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "DuLieuLoXien3Object.h"

@implementation DuLieuLoXien3Object

- (instancetype) initWithName: (NSString *) name
                      number1: (NSString *) number1
                      number2: (NSString *) number2
                      number3: (NSString *) number3
                        point: (NSString *) point
                         date: (NSString *) date;
{
    self = [super init];
    
    if (self) {
        _name = name;
        _number1 = number1;
        _number2 = number2;
        _number3 = number3;
        _point = point;
        _date = date;
    }
    return self;
}

@end
