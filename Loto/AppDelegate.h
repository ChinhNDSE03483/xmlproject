//
//  AppDelegate.h
//  Loto
//
//  Created by Chính Nguyen on 7/3/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    NSString *role;
    NSString *userName;
    NSString *password;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSString *role;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *password;
@end

