//
//  ClientViewController.m
//  Loto
//
//  Created by Chính Nguyen on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "ClientViewController.h"
#import "Constraint.h"
#import <AFNetworking.h>
#import <ProgressHUD.h>
#import "XMLReader.h"
#import "ClientObject.h"
#import "ClientTableViewCell.h"
#import "DetailClientViewController.h"
#import "AppDelegate.h"
#import "AddClientViewController.h"
@interface ClientViewController ()

@end

@implementation ClientViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",((AppDelegate *)([UIApplication sharedApplication].delegate)).role);
    if([((AppDelegate *)([UIApplication sharedApplication].delegate)).role isEqualToString:@"1"]){
        
    }else{
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    UIColor *color = [UIColor colorWithRed:24.0 / 255 green:75.0 / 255 blue:152.0 / 255 alpha:1];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.1608 green:0.7333 blue:0.6118 alpha:1.0];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    // Do any additional setup after loading the view.
    _tbvClient.delegate = self;
    _tbvClient.dataSource = self;
    _tbvClient.tableFooterView = [[UIView alloc] init];
    _viewCenter.backgroundColor = kColorGrey300;
    _viewUnder.backgroundColor = kColorGrey300;
    _xmlDict = [[NSDictionary alloc] init];
    _listClient = [[NSMutableArray alloc] init];
    [self getDataFromServer];
    NSLog(@"Data Count: %ld", [_listClient count]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_listClient count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"ClientTableViewCell";
    ClientTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"ClientTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    ClientObject *clientObject = _listClient[indexPath.row];
    
    [cell displayCellWithObject:clientObject];
    
    cell.preservesSuperviewLayoutMargins = NO;
    cell.separatorInset = UIEdgeInsetsZero;
    cell.layoutMargins = UIEdgeInsetsZero;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DetailClientViewController *mainView = [storyboard instantiateViewControllerWithIdentifier:@"DetailClientViewController"];
    mainView.clientObject = _listClient[indexPath.row];
    
    [self.navigationController pushViewController:mainView animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (void) getDataFromServer {
    NSString *message = [[NSString alloc] initWithFormat:@"<?xml version=\"1.0\" ?>\n<user><username>%@</username><password>%@</password></user>",((AppDelegate *)([UIApplication sharedApplication].delegate)).userName,((AppDelegate *)([UIApplication sharedApplication].delegate)).password];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:kURL, @"get/all/clients"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu",(unsigned long)[message length]];
    
    [request addValue:@"application/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request addValue:msgLength                         forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[message dataUsingEncoding:NSUTF8StringEncoding]];
    NSData *response = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:nil error:nil];
    //    NSLog(@"Response Ghi De: %@",[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding]);
    
    _xmlDict = [XMLReader dictionaryForXMLString:[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding] error:nil];
    NSLog(@"%@",_xmlDict);
    [_listClient removeAllObjects];
    
    for (NSDictionary *dict in _xmlDict[@"users"][@"user"]) {
        
        NSString *userName = [[[NSString stringWithFormat:@"%@", dict[@"username"][@"text"]]
                           stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                          stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *password = [[[NSString stringWithFormat:@"%@", dict[@"password"][@"text"]]
                             stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                            stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *fullName = [[NSString stringWithFormat:@"%@", dict[@"fullName"][@"text"]]
                            stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        NSString *phoneNumber = [[[NSString stringWithFormat:@"%@", dict[@"phoneNumber"][@"text"]]
                           stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                          stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *role = [[[NSString stringWithFormat:@"%@", dict[@"role"][@"text"]]
                           stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                          stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *success = [[[NSString stringWithFormat:@"%@", dict[@"success"][@"text"]]
                           stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                          stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *createdBy = [[[NSString stringWithFormat:@"%@", dict[@"createdBy"][@"text"]]
                              stringByReplacingOccurrencesOfString:@"\n" withString:@""]
                             stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        ClientObject *clientObject = [[ClientObject alloc] initWithUsername:userName
                                                                   password:password
                                                                   fullName:fullName
                                                                phoneNumber:phoneNumber
                                                                       role:role
                                                                    success:success
                                                                  createdBy:createdBy];
        [_listClient addObject:clientObject];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnAdd:(id)sender {
    NSLog(@"add btn");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddClientViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"AddClientViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}
@end
