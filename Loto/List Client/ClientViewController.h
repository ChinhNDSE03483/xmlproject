//
//  ClientViewController.h
//  Loto
//
//  Created by Chính Nguyen on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClientViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tbvClient;
@property (strong, nonatomic) NSDictionary *xmlDict;
@property (weak, nonatomic) IBOutlet UIView *viewCenter;
@property (weak, nonatomic) IBOutlet UIView *viewUnder;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnAdd;
- (IBAction)btnAdd:(id)sender;
@property (strong, nonatomic) NSMutableArray *listClient;
@end
