//
//  DetailClientViewController.m
//  Loto
//
//  Created by Chính Nguyen on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "DetailClientViewController.h"

@interface DetailClientViewController ()

@end

@implementation DetailClientViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_viewFullName.layer setCornerRadius:5.0f];
    [_viewPhoneNumber.layer setCornerRadius:5.0f];
    [_viewUsername.layer setCornerRadius:5.0f];
    [_viewRole.layer setCornerRadius:5.0f];
    
    NSArray *comps = [_clientObject.fullName componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSMutableArray *words = [NSMutableArray array];
    for(NSString *comp in comps) {
        if([comp length] > 1) {
            [words addObject:comp];
        }
    }
    NSString *result = [words componentsJoinedByString:@" "];
    
    _lblFullName.text = result;
    _lblPhoneNumber.text = _clientObject.phoneNumber;
    _lblUserName.text = _clientObject.username;
    if([_clientObject.role isEqualToString:@"0"]){
        _lblRole.text = @"Super Admin";
    }else if([_clientObject.role isEqualToString:@"1"]){
        _lblRole.text = @"Admin";
    }else if([_clientObject.role isEqualToString:@"2"]){
        _lblRole.text = @"Client";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
