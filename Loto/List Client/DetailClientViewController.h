//
//  DetailClientViewController.h
//  Loto
//
//  Created by Chính Nguyen on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClientObject.h"

@interface DetailClientViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *viewFullName;
@property (weak, nonatomic) IBOutlet UILabel *lblFullName;

@property (weak, nonatomic) IBOutlet UIView *viewPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblPhoneNumber;


@property (weak, nonatomic) IBOutlet UIView *viewUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;

@property (weak, nonatomic) IBOutlet UIView *viewRole;
@property (weak, nonatomic) IBOutlet UILabel *lblRole;

@property (weak, nonatomic) ClientObject *clientObject;
@end
