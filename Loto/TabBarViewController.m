//
//  TabBarViewController.m
//  Loto
//
//  Created by Chính Nguyen on 7/3/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "TabBarViewController.h"
#import "FadeAnimationController.h"

@interface TabBarViewController ()
@property (nonatomic, strong) FadeAnimationController *fadeAnimationController;
@end


@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBar.backgroundImage = [UIImage new];
    self.tabBar.shadowImage = [UIImage new];
    
    for (UITabBarItem *tabBarItem in self.tabBar.items) {
        tabBarItem.image = [tabBarItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        tabBarItem.selectedImage = [tabBarItem.selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    }
    self.fadeAnimationController = [[FadeAnimationController alloc] init];
}

- (CGFloat)floatingContentHeight {
    return 500.0f / 568.0f * self.view.bounds.size.height;
}

- (id <UIViewControllerAnimatedTransitioning>)tabBarController:(UITabBarController *)tabBarController animationControllerForTransitionFromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    id <UIViewControllerAnimatedTransitioning> animationController = [super tabBarController:tabBarController animationControllerForTransitionFromViewController:fromVC toViewController:toVC];
    if (!animationController) {
        animationController = self.fadeAnimationController;
        self.fadeAnimationController.transitionDuration = self.transitionDuration;
    }
    return animationController;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
