//
//  DuLieuGhiDeTableViewCell.m
//  Loto
//
//  Created by Dai Trinh on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "DuLieuLoXien4TableViewCell.h"

@implementation DuLieuLoXien4TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) displayCellWithObject : (DuLieuLoXien4Object *) duLieuLoXien4Object {
    _viewLine1.backgroundColor = kColorGrey300;
    _viewLine2.backgroundColor = kColorGrey300;
    _viewLine3.backgroundColor = kColorGrey300;
    _viewLine4.backgroundColor = kColorGrey300;
    _viewLine5.backgroundColor = kColorGrey300;
    _viewLine6.backgroundColor = kColorGrey300;
    
    _lblName.text = duLieuLoXien4Object.name;
    _lblNumber1.text = duLieuLoXien4Object.number1;
    _lblNumber2.text = duLieuLoXien4Object.number2;
    _lblNumber3.text = duLieuLoXien4Object.number3;
    _lblNumber4.text = duLieuLoXien4Object.number4;
    _lblPoint.text = duLieuLoXien4Object.point;
    _lblDate.text = duLieuLoXien4Object.date;
}

@end
