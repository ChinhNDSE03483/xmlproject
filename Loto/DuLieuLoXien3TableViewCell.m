//
//  DuLieuGhiDeTableViewCell.m
//  Loto
//
//  Created by Dai Trinh on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "DuLieuLoXien3TableViewCell.h"

@implementation DuLieuLoXien3TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) displayCellWithObject : (DuLieuLoXien3Object *) duLieuLoXien3Object {
    _viewLine1.backgroundColor = kColorGrey300;
    _viewLine2.backgroundColor = kColorGrey300;
    _viewLine3.backgroundColor = kColorGrey300;
    _viewLine4.backgroundColor = kColorGrey300;
    _viewLine5.backgroundColor = kColorGrey300;
    
    _lblName.text = duLieuLoXien3Object.name;
    _lblNumber1.text = duLieuLoXien3Object.number1;
    _lblNumber2.text = duLieuLoXien3Object.number2;
    _lblNumber3.text = duLieuLoXien3Object.number3;
    _lblPoint.text = duLieuLoXien3Object.point;
    _lblDate.text = duLieuLoXien3Object.date;
}

@end
