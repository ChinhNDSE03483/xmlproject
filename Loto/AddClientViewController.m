//
//  AddClientViewController.m
//  Loto
//
//  Created by Chính Nguyen on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "AddClientViewController.h"
#import "ACFloatingTextField.h"
#import "Constraint.h"
#import "AFNetWorking.h"
#import "ProgressHUD.h"
#import "XMLReader.h"
#import "AppDelegate.h"

@interface AddClientViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtFullName;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtUserName;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtPassword;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtPhoneNumber;

@end

@implementation AddClientViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _txtFullName.backgroundColor = [UIColor whiteColor];
    _txtUserName.backgroundColor = [UIColor whiteColor];
    _txtPassword.backgroundColor = [UIColor whiteColor];
    _txtPhoneNumber.backgroundColor = [UIColor whiteColor];
    
    _txtFullName.textColor = [UIColor colorWithRed:0.4667 green:0.7020 blue:0.6000 alpha:1];
    _txtUserName.textColor = [UIColor colorWithRed:0.4667 green:0.7020 blue:0.6000 alpha:1];
    _txtPassword.textColor = [UIColor colorWithRed:0.4667 green:0.7020 blue:0.6000 alpha:1];
    _txtPhoneNumber.textColor = [UIColor colorWithRed:0.4667 green:0.7020 blue:0.6000 alpha:1];
    
    
    _txtFullName.selectedLineColor = [UIColor colorWithRed:0.4667 green:0.7020 blue:0.6000 alpha:1];
    _txtFullName.selectedPlaceHolderColor = [UIColor colorWithRed:0.4667 green:0.7020 blue:0.6000 alpha:1];
    _txtUserName.selectedLineColor = [UIColor colorWithRed:0.4667 green:0.7020 blue:0.6000 alpha:1];
    _txtUserName.selectedPlaceHolderColor = [UIColor colorWithRed:0.4667 green:0.7020 blue:0.6000 alpha:1];
    _txtPassword.selectedLineColor = [UIColor colorWithRed:0.4667 green:0.7020 blue:0.6000 alpha:1];
    _txtPassword.selectedPlaceHolderColor = [UIColor colorWithRed:0.4667 green:0.7020 blue:0.6000 alpha:1];
    _txtPhoneNumber.selectedLineColor = [UIColor colorWithRed:0.4667 green:0.7020 blue:0.6000 alpha:1];
    _txtPhoneNumber.selectedPlaceHolderColor = [UIColor colorWithRed:0.4667 green:0.7020 blue:0.6000 alpha:1];
    
    _txtFullName.placeHolderColor = [UIColor lightGrayColor];
    _txtFullName.lineColor = [UIColor lightGrayColor];
    _txtUserName.placeHolderColor = [UIColor lightGrayColor];
    _txtUserName.lineColor = [UIColor lightGrayColor];
    _txtPassword.placeHolderColor = [UIColor lightGrayColor];
    _txtPassword.lineColor = [UIColor lightGrayColor];
    _txtPhoneNumber.placeHolderColor = [UIColor lightGrayColor];
    _txtPhoneNumber.lineColor = [UIColor lightGrayColor];
    
    _txtPassword.secureTextEntry = TRUE;
    // Do any additional setup after loading the view.
}

#pragma mark  UITextfield Delegates
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return true;
}
- (IBAction)btnRegister:(id)sender {
    NSString *phoneNumber = self.txtPhoneNumber.text;
    NSString *password = self.txtPassword.text;
    NSString *userName = self.txtUserName.text;
    NSString *fullName = self.txtFullName.text;
    if ([phoneNumber length] == 0)	{
        [ProgressHUD showError:@"Bạn chưa điền số điện thoại."];
        return;
    }
    if ([userName length] == 0)	{
        [ProgressHUD showError:@"Bạn chưa điền tên tài khoản."];
        return;
    }
    if ([fullName length] == 0)	{
        [ProgressHUD showError:@"Bạn chưa điền họ tên."];
        return;
    }
    if ([password length] == 0)	{
        [ProgressHUD showError:@"Bạn chưa điền mật khẩu."];
        return;
    }
    
    [ProgressHUD show:@"Vui lòng đợi..."];
    NSString *message = [[NSString alloc] initWithFormat:@"<?xml version=\"1.0\" ?>\n<NewUser>\n<requestByUser>%@</requestByUser><username>%@</username>\n<password>%@</password><fullName>%@</fullName><phoneNumber>%@</phoneNumber></NewUser>",((AppDelegate *)([UIApplication sharedApplication].delegate)).userName,userName,password,fullName,phoneNumber];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:kURL, @"create/client"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu",(unsigned long)[message length]];
    
    [request addValue:@"application/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request addValue:msgLength                         forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[message dataUsingEncoding:NSUTF8StringEncoding]];
    NSData *response = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:nil error:nil];
    
    //    NSLog(@"Response: %@",[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding]);
    
    NSDictionary* xmlDict = [XMLReader dictionaryForXMLString:[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding] error:nil];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:xmlDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSLog(@"Final %@",jsonString);
    }
    
    
    NSString *status = xmlDict[@"response"][@"status"][@"text"];
    
    status = [[status stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    NSLog(@"%@",status);
    if([status isEqualToString:@"true"]){
         [ProgressHUD showSuccess:xmlDict[@"response"][@"message"][@"text"]];
    }else{
        [ProgressHUD showError:xmlDict[@"response"][@"message"][@"text"]];
    }
    
    
    
//    if([success isEqualToString:@"true"]){
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        TabBarViewController *tabBarViewController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
//        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//        ((AppDelegate *)([UIApplication sharedApplication].delegate)).role = role;
//        //        NavigationController *navigationController =
//        //        [[NavigationController alloc] initWithRootViewController:tabBarViewController];
//        [self presentViewController:tabBarViewController
//                           animated:YES
//                         completion:^{
//                         }];
//        [ProgressHUD dismiss];
//        [appDelegate.window makeKeyAndVisible];
//    }
//    //    } else if([success isEqualToString:@"true"] && ([role isEqualToString:@"1"] || [role isEqualToString:@"0"])){
//    //       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    //       TabBarViewController *tabBarViewController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
//    //       AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    //       //        NavigationController *navigationController =
//    //       //        [[NavigationController alloc] initWithRootViewController:tabBarViewController];
//    //       [self presentViewController:tabBarViewController
//    //                          animated:YES
//    //                        completion:^{
//    //                        }];
//    //       [ProgressHUD dismiss];
//    //       [appDelegate.window makeKeyAndVisible];
//    //    }
//    else{
//        [ProgressHUD showError:@"Sai tên đăng nhập hoặc mật khẩu!"];
//    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
