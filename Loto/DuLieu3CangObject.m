//
//  DuLieu3CangObject.m
//  Loto
//
//  Created by Dai Trinh on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "DuLieu3CangObject.h"

@implementation DuLieu3CangObject

- (instancetype) initWithName: (NSString *) name
                       number: (NSString *) number
                        price: (NSString *) price
                         date: (NSString *) date
{
    self = [super init];
    
    if (self) {
        _name = name;
        _number = number;
        _price = price;
        _date = date;
    }
    return self;
}

@end
