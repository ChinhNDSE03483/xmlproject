//
//  ClientObject.h
//  Loto
//
//  Created by Chính Nguyen on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClientObject : NSObject
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *role;
@property (nonatomic, strong) NSString *success;
@property (nonatomic, strong) NSString *createdBy;

- (instancetype) initWithUsername: (NSString *) username
                         password: (NSString *) password
                         fullName: (NSString *) fullName
                      phoneNumber: (NSString *) phoneNumber
                             role: (NSString *) role
                          success: (NSString *) success
                        createdBy: (NSString *) createdBy;
@end
