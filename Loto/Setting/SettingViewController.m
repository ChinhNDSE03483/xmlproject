//
//  SettingViewController.m
//  youtube
//
//  Created by Chính Nguyen on 5/22/17.
//  Copyright © 2017 Chính Nguyen. All rights reserved.
//

#import "SettingViewController.h"
#import "LUNTabBarController.h"
#import "AboutUsViewController.h"
#import "NavigationViewController.h"

@interface SettingViewController ()
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIButton *buttonClose;

@property (nonatomic, strong) NSArray<NSString *> *titles;

@property (nonatomic, strong) NSArray<UIImage *> *images;
@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titles = @[
                    
                    @"Rate app",
                    @"Settings",
                    @"About us"
                    ];
    
    self.images = @[
                    [UIImage imageNamed:@"iconTermsOfUse"],
                    [UIImage imageNamed:@"iconSettings"],
                    [UIImage imageNamed:@"iconEmailUs"]
                    ];
}

- (IBAction)closeClicked:(id)sender {
    LUNTabBarController *tabBarController = (LUNTabBarController *) self.tabBarController;
    [tabBarController hideFloatingTab];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MenuTableViewCell"];
    cell.textLabel.text = self.titles[(NSUInteger) indexPath.row];
    cell.imageView.image = self.images[(NSUInteger) indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%ld",indexPath.row);
    if(indexPath.row == 2){
        NavigationViewController *nav = [[NavigationViewController alloc]init];
        AboutUsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsViewController"];
        [nav pushViewController:vc animated:YES];
        [self presentViewController:nav animated:YES completion:nil];
    }
    
}



#pragma mark - LUNTabBarFloatingControllerAnimatedTransitioning

- (void)floatingViewControllerStartedAnimatedTransition:(BOOL)isPresenting {
    CGFloat angle = (CGFloat) (isPresenting ? -M_PI_2 : M_PI_2);
    self.buttonClose.transform = CGAffineTransformMakeRotation(angle);
    self.buttonClose.alpha = isPresenting ? 0 : 1;
}

- (void (^)(void))keyframeAnimationForFloatingViewControllerAnimatedTransition:(BOOL)isPresenting {
    return ^{
        UIColor *backgroundColor = isPresenting ? [[UIColor whiteColor] colorWithAlphaComponent:0.95] : [UIColor whiteColor];
        self.contentView.backgroundColor = backgroundColor;
        
        [UIView addKeyframeWithRelativeStartTime:isPresenting ? 0.8 : 0
                                relativeDuration:0.2
                                      animations:^{
                                          self.buttonClose.transform = CGAffineTransformIdentity;
                                          self.buttonClose.alpha = isPresenting ? 1 : 0;
                                      }];
    };
}


- (void)floatingViewControllerFinishedAnimatedTransition:(BOOL)isPresenting wasCompleted:(BOOL)wasCompleted {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
