//
//  LoginViewController.h
//  Loto
//
//  Created by Chính Nguyen on 7/3/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
- (IBAction)btnLogin:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *fieldPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *fieldPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPassword;
- (IBAction)btnForgotPassword:(id)sender;

@end
