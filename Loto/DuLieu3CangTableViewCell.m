//
//  DuLieuGhiDeTableViewCell.m
//  Loto
//
//  Created by Dai Trinh on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "DuLieu3CangTableViewCell.h"

@implementation DuLieu3CangTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) displayCellWithObject : (DuLieu3CangObject *) duLieu3CangObject {
    _viewLine1.backgroundColor = kColorGrey300;
    _viewLine2.backgroundColor = kColorGrey300;
    _viewLine3.backgroundColor = kColorGrey300;
    
    _lblName.text = duLieu3CangObject.name;
    _lblNumber.text = duLieu3CangObject.number;
    _lblPrice.text = duLieu3CangObject.price;
    _lblDate.text = duLieu3CangObject.date;
}

@end
