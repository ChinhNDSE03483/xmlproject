//
//  ClientTableViewCell.h
//  Loto
//
//  Created by Chính Nguyen on 7/6/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constraint.h"
#import "ClientObject.h"

@interface ClientTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIView *viewCenter;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
- (void) displayCellWithObject : (ClientObject *) clientObject;
@end
