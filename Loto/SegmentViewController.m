//
//  SegmentViewController.m
//  Loto
//
//  Created by Chính Nguyen on 7/5/17.
//  Copyright © 2017 FPT. All rights reserved.
//

#import "SegmentViewController.h"
#import <CarbonKit/CarbonKit.h>
#import "DuLieuGhiDeViewController.h"
#import "DuLieu3CangViewController.h"
#import "DuLieuGhiLoViewController.h"
#import "DuLieuLoXien2ViewController.h"
#import "DuLieuLoXien3ViewController.h" 
#import "DuLieuLoXien4ViewController.h"


@interface SegmentViewController ()<CarbonTabSwipeNavigationDelegate>{
    NSArray *items;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
}

@end

@implementation SegmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = @"CarbonKit";
    
    items = @[
              @"Dữ liệu ghi đề",
              @"Dữ liệu ghi 3 càng",
              @"Dữ liệu ghi lô",
              @"Dữ liệu ghi lô xiên 2",
              @"Dữ liệu ghi lô xiên 3",
              @"Dữ liệu ghi lô xiên 4"
              ];
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    
    [self style];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)style {
    
    UIColor *color = [UIColor colorWithRed:24.0 / 255 green:75.0 / 255 blue:152.0 / 255 alpha:1];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.1608 green:0.7333 blue:0.6118 alpha:1.0];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    [carbonTabSwipeNavigation setIndicatorColor:[UIColor colorWithRed:0.1608 green:0.7333 blue:0.6118 alpha:1.0]];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
//    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:80 forSegmentAtIndex:0];
//    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:80 forSegmentAtIndex:1];
//    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:80 forSegmentAtIndex:2];
    
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[color colorWithAlphaComponent:0.6]
                                        font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setSelectedColor:[UIColor colorWithRed:0.1608 green:0.7333 blue:0.6118 alpha:1.0] font:[UIFont boldSystemFontOfSize:14]];
}

#pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    switch (index) {
        case 0:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"DuLieuGhiDeViewController"];
        case 1:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"DuLieu3CangViewController"];
        case 2:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"DuLieuGhiLoViewController"];
        case 3:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"DuLieuLoXien2ViewController"];
        case 4:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"DuLieuLoXien3ViewController"];
        default:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"DuLieuLoXien4ViewController"];
    }
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
//    switch (index) {
//        case 0:
//            
//            self.title = @"Dữ liệu ghi đề";
//            break;
//        case 1:
//            self.title = @"Dữ liệu ghi 3 càng";
//            break;
//        case 2:
//            self.title = @"Dữ liệu ghi lô";
//            break;
//        case 3:
//            self.title = @"Dữ liệu ghi lô xiên 2";
//            break;
//        case 4:
//            self.title = @"Dữ liệu ghi lô xiên 3";
//            break;
//        case 5:
//            self.title = @"Dữ liệu ghi lô xiên 4";
//            break;
//        default:
//            self.title = items[index];
//            break;
//    }
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    NSLog(@"Did move at index: %ld", index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}

@end
